import {POKEMON_TYPE_COLORS} from './metrics';

export function isDarkColor(color) {
  // Variables for red, green, blue values
  let r = null,
    g = null,
    b = null,
    hsp = null;

  // Check the format of the color, HEX or RGB?
  if (color.match(/^rgb/)) {
    // If HEX --> store the red, green, blue values in separate variables
    color = color.match(
      /^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/
    );

    r = color[1];
    g = color[2];
    b = color[3];
  } else {
    // If RGB --> Convert it to HEX: http://gist.github.com/983661
    color = +`0x${color.slice(1).replace(color.length < 5 && /./g, '$&$&')}`;

    r = color >> 16;
    g = (color >> 8) & 255;
    b = color & 255;
  }

  // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
  const hspRed = 0.299 * (r * r);
  const hspGreen = 0.587 * (g * g);
  const hspBlue = 0.114 * (b * b);
  hsp = Math.sqrt(hspRed + hspGreen + hspBlue);

  // Using the HSP value, determine whether the color is light or dark
  return hsp <= 127.5;
}

export function getPokemonBackgroundColor(types) {
  return types.map(type => POKEMON_TYPE_COLORS[type]);
}

export function getPokemonNumber(id) {
  const str = `${id}`;
  const pad = '000';
  return pad.substring(0, pad.length - str.length) + str;
}
