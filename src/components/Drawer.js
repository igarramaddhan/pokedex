// @flow
import React from 'react';
import styled from 'styled-components';
import Close from 'react-feather/dist/icons/x';
import {Link} from 'react-router-dom';
import DrawerContext from '../DrawerContex';
import pokemons from '../pokemons';

const DrawerContainer = styled.div`
  top: 0;
  left: ${props => (props.active ? 0 : '-300px')};
  z-index: 20;
  width: 300px;
  height: 100vh;
  position: fixed;
  background-color: white;
  transition: 250ms ease-in;

  h3 {
    margin: 0;
  }
`;

const DrawerOverlay = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 19;
  width: 100%;
  height: 100vh;
  position: fixed;
  transition: 250ms ease-in;
  background-color: rgba(0, 0, 0, 0.5);
  display: ${props => (props.active ? 'flex' : 'none')};
`;

const TopContent = styled.div`
  height: 56px;
  display: flex;
  padding: 0 16px;
  align-items: center;
  flex-direction: row;
  border-bottom: 1px solid #eee;
  justify-content: space-between;
`;

const IconContainer = styled.div`
  cursor: pointer;
`;

const DrawerContent = styled.div`
  display: flex;
  flex-direction: column;
`;

const Input = styled.input`
  border: none;
  height: 30px;
  margin: 16px;
  font-size: 16px;
  padding: 4px 8px;
  align-self: center;
  border-radius: 8px;
  background-color: #eee;
  width: calc(100% - 48px);
`;

const PokemonItem = styled(Link)`
  flex: 1;
  cursor: pointer;
  text-decoration: none;
  color: black;
  &:hover {
    opacity: 0.5;
    font-weight: bold;
  }
`;

const Drawer = () => {
  const context = React.useContext(DrawerContext);
  const [search, setSearch] = React.useState('');
  return (
    <>
      <DrawerOverlay active={context.active} onClick={context.toggleDrawer} />
      <DrawerContainer active={context.active}>
        <TopContent>
          <h3>Search</h3>
          <IconContainer>
            <Close onClick={context.toggleDrawer} />
          </IconContainer>
        </TopContent>
        <DrawerContent>
          <Input
            placeholder="Type to search..."
            value={search}
            onChange={e => setSearch(e.target.value)}
          />
          <ul>
            {pokemons
              .filter(pokemon =>
                pokemon.name.toLowerCase().includes(search.toLowerCase())
              )
              .map((pokemon, idx) => (
                <li key={idx} onClick={context.toggleDrawer}>
                  <PokemonItem to={`/pokemon/${pokemon.id}`}>
                    {pokemon.name}
                  </PokemonItem>
                </li>
              ))}
          </ul>
        </DrawerContent>
      </DrawerContainer>
    </>
  );
};
export default Drawer;
