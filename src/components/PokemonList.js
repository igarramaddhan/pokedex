// @flow
import React from 'react';
import styled from 'styled-components';
import {type Pokemon} from '../types';
import PokemonCard from './PokemonCard';

const ListContainer = styled.div`
  flex: 1;
  padding: 0;
  display: flex;
  flex-wrap: wrap;
  list-style-type: none;

  li {
    flex: 0.5;
    display: flex;

    @media only screen and (max-width: 390px) {
      flex: 1;
    }
  }
`;

type Props = {
  pokemons: Pokemon[],
};

const PokemonList = (props: Props) => (
  <ListContainer>
    {props.pokemons.map(val => (
      <li key={val.id}>
        <PokemonCard pokemon={val} />
      </li>
    ))}
  </ListContainer>
);

export default PokemonList;
