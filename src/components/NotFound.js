// @flow
import React from 'react';
import styled from 'styled-components';

const NotFoundContainer = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const NotFoundText = styled.div`
  font-size: 18px;
`;

type Props = {
  text: string,
};

const NotFound = (props: Props) => (
  <NotFoundContainer>
    <NotFoundText>{props.text}</NotFoundText>
  </NotFoundContainer>
);

export default NotFound;
