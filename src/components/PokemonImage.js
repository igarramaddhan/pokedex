import styled from 'styled-components';

export const PokemonImageContainer = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const PokemonImage = styled.img`
  width: 150px;
  height: 150px;
  object-fit: contain;
`;
