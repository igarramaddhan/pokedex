// @flow
import * as React from 'react';
import styled from 'styled-components';
import Menu from 'react-feather/dist/icons/menu';
import Back from 'react-feather/dist/icons/arrow-left';
import {withRouter, RouteComponentProps} from 'react-router-dom';
import {isDarkColor} from '../utils/helpers';
import DrawerContext from '../DrawerContex';

const HeaderContainer = styled.div`
  top: 0;
  left: 0;
  right: 0;
  z-index: 10;
  height: 56px;
  display: flex;
  position: fixed;
  flex-direction: row;
  justify-content: center;
`;

const HeaderContentContainer = styled.div`
  flex: 1;
  display: flex;
  max-width: 480px;
  flex-direction: row;
  justify-content: space-between;
  background-color: ${props =>
    props.backgroundColor ? props.backgroundColor : 'white'};
`;

const IconContainer = styled.a`
  width: 56px;
  height: 56px;
  display: flex;
  cursor: pointer;
  align-items: center;
  justify-content: center;
`;

export type HeaderProps = {
  leftIcon?: typeof Menu | null,
  rightIcon?: typeof Menu | null,
  backgroundColor?: string | null,
} & RouteComponentProps;

const Header = (props: HeaderProps) => {
  const context = React.useContext(DrawerContext);
  const color = props.backgroundColor
    ? isDarkColor(props.backgroundColor)
      ? '#fff'
      : '#000'
    : '#000';
  return (
    <HeaderContainer>
      <HeaderContentContainer backgroundColor={props.backgroundColor}>
        <IconContainer>
          {props.leftIcon ? (
            props.leftIcon
          ) : props.location.pathname === '/' ? (
            <Menu color={color} onClick={context.toggleDrawer} />
          ) : (
            <Back color={color} onClick={() => props.history.goBack()} />
          )}
        </IconContainer>
        <IconContainer>{props.rightIcon && props.rightIcon}</IconContainer>
      </HeaderContentContainer>
    </HeaderContainer>
  );
};

export default withRouter(Header);
