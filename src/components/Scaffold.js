// @flow
import React from 'react';
import styled from 'styled-components';

import Header, {type HeaderProps} from './Header.js';
import Drawer from './Drawer.js';

const ScaffoldContainer = styled.div`
  width: 480px;
  display: flex;
  max-width: 480px;
  padding-top: 56px;
  position: relative;
  align-self: center;
  flex-direction: column;
  background-color: #fff;
  min-height: calc(100vh - 56px);

  @media only screen and (max-width: 480px) {
    flex: 1;
    width: 100%;
  }
`;

type Props = {
  children: any,
  headerStyle?: HeaderProps | null,
};

const Scaffold = (props: Props) => {
  const headerProps = props.headerStyle ? props.headerStyle : {};
  return (
    <ScaffoldContainer>
      <Drawer />
      <Header {...headerProps} />
      {props.children}
    </ScaffoldContainer>
  );
};

export default Scaffold;
