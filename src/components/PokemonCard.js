// @flow
import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {type Pokemon} from '../types';
import {
  getPokemonBackgroundColor,
  isDarkColor,
  getPokemonNumber,
} from '../utils/helpers';
import {
  PokemonType,
  PokemonTypeContainer,
  PokemonTypeBackground,
  PokemonTypeBackgroundContainer,
} from './PokemonType';
import {PokemonImageContainer, PokemonImage} from './PokemonImage';

const PokemonCardContainer = styled(Link)`
  flex: 1;
  margin: 8px;
  padding: 8px;
  display: flex;
  cursor: pointer;
  min-height: 250px;
  position: relative;
  text-decoration: none;
  &:hover {
    opacity: 0.5;
  }
`;

const PokemonCardContent = styled.div`
  flex: 1;
  z-index: 1;
  display: flex;
  flex-direction: column;
`;

const PokemonName = styled.h3`
  margin: 0;
  display: block;
  font-size: 24px;
  padding: 4px 8px;
  font-weight: bold;
  margin-bottom: 4px;
  border-radius: 20px;
  color: ${props => (isDarkColor(props.background) ? 'white' : 'black')};
`;

const PokemonNumber = styled.span`
  font-weight: bold;
  align-self: flex-end;
  color: ${props => (isDarkColor(props.background) ? 'white' : 'black')};
`;

type Props = {
  pokemon: Pokemon,
};

const PokemonCard = ({pokemon}: Props) => {
  const backgrounds = getPokemonBackgroundColor(pokemon.types);
  const pokemonNumber = getPokemonNumber(pokemon.id);
  return (
    <PokemonCardContainer to={`/pokemon/${pokemon.id}`}>
      <PokemonTypeBackgroundContainer>
        {backgrounds.map((val, idx) => (
          <PokemonTypeBackground key={idx} backgroundColor={val} />
        ))}
      </PokemonTypeBackgroundContainer>
      <PokemonCardContent>
        <PokemonName background={backgrounds[0]}>{pokemon.name}</PokemonName>
        <PokemonTypeContainer>
          {pokemon.types.map((val, idx) => (
            <PokemonType key={idx}>{val}</PokemonType>
          ))}
        </PokemonTypeContainer>
        <PokemonImageContainer>
          <PokemonImage src={pokemon.image} alt="pokemon-image" />
        </PokemonImageContainer>
        <PokemonNumber background={backgrounds[backgrounds.length - 1]}>
          #{pokemonNumber}
        </PokemonNumber>
      </PokemonCardContent>
    </PokemonCardContainer>
  );
};

export default PokemonCard;
