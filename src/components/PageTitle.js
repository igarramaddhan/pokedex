import styled from 'styled-components';

const PageTitle = styled.h2`
  margin: 0;
  padding: 8px;
`;

export default PageTitle;
