import styled from 'styled-components';

export const PokemonTypeBackgroundContainer = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  position: absolute;
`;

export const PokemonTypeBackground = styled.div`
  flex: 1;
  background-color: ${props =>
    props.backgroundColor ? props.backgroundColor : '#fff'};
`;

export const PokemonTypeContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const PokemonType = styled.span`
  color: black;
  font-size: 11px;
  margin: 2px 4px;
  padding: 4px 8px;
  background-color: white;
`;
