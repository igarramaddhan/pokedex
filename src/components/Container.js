import styled from 'styled-components';

const Container = styled.div`
  flex: 1;
  color: black;
  display: flex;
  flex-direction: column;
  padding: 4px 8px;
`;

export default Container;
