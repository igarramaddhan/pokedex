// @flow
export type Pokemon = {
  id: number,
  image: string,
  name: string,
  types: string[],
  caught: boolean,
  species: string,
  height: number,
  weight: number,
  abilities: string[],
};
