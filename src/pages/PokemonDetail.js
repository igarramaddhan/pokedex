// @flow
import React from 'react';
import styled from 'styled-components';
import {RouteComponentProps} from 'react-router-dom';
import Scaffold from '../components/Scaffold';
import Container from '../components/Container';
import PageTitle from '../components/PageTitle';
import pokemons from '../pokemons';
import NotFound from '../components/NotFound';
import {
  isDarkColor,
  getPokemonNumber,
  getPokemonBackgroundColor,
} from '../utils/helpers';
import {
  PokemonType,
  PokemonTypeContainer,
  PokemonTypeBackground,
  PokemonTypeBackgroundContainer,
} from '../components/PokemonType';
import {PokemonImageContainer, PokemonImage} from '../components/PokemonImage';
import PokemonList from '../components/PokemonList';

const TopContainer = styled.div`
  height: 300px;
  display: flex;
  position: relative;
  background-color: ${props =>
    props.backgroundColor ? props.backgroundColor : 'white'};
`;

const TopContent = styled.div`
  flex: 1;
  z-index: 1;
  display: flex;
  padding: 4px 8px;
  flex-direction: column;
`;

const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const PokemonNumber = styled.span`
  font-weight: bold;
  margin-right: 8px;
  color: ${props => (isDarkColor(props.background) ? 'white' : 'black')};
`;

const SectionContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 16px 0;
  padding: 0 8px;
`;

const Section = styled.div`
  display: flex;
  margin: 4px;
  width: calc(50% - 8px);
`;

const SectionName = styled.p`
  flex: 1;
  margin: 0;
`;

const SectionValue = styled.p`
  flex: 1;
  margin: 0;
  font-weight: bold;
`;

const BottomTitle = styled.h3`
  margin: 8px;
`;

type Props = {} & RouteComponentProps;

const sections = ['Species', 'Height', 'Weight', 'Abilities'];

const PokemonDetail = ({match}: Props) => {
  const pokemon = pokemons.find(val => `${val.id}` === match.params.id);
  const backgrounds = getPokemonBackgroundColor(pokemon ? pokemon.types : []);
  const textColor = isDarkColor(backgrounds[0]) ? 'white' : 'black';
  return (
    <Scaffold headerStyle={{backgroundColor: pokemon ? backgrounds[0] : null}}>
      {!pokemon ? (
        <NotFound text="Oops, pokemon not found" />
      ) : (
        <>
          <TopContainer>
            <PokemonTypeBackgroundContainer>
              {backgrounds.map((val, idx) => (
                <PokemonTypeBackground key={idx} backgroundColor={val} />
              ))}
            </PokemonTypeBackgroundContainer>
            <TopContent>
              <TitleContainer>
                <PageTitle style={{color: textColor}}>{pokemon.name}</PageTitle>
                <PokemonNumber background={backgrounds[backgrounds.length - 1]}>
                  #{getPokemonNumber(pokemon.id)}
                </PokemonNumber>
              </TitleContainer>
              <PokemonTypeContainer style={{margin: '0 4px'}}>
                {pokemon.types.map((val, idx) => (
                  <PokemonType style={{fontSize: '14px'}} key={idx}>
                    {val}
                  </PokemonType>
                ))}
              </PokemonTypeContainer>
              <PokemonImageContainer style={{transform: 'translateY(30%)'}}>
                <PokemonImage
                  style={{transform: 'scale(1.5)'}}
                  src={pokemon.image}
                  alt="pokemon-image"
                />
              </PokemonImageContainer>
            </TopContent>
          </TopContainer>
          <Container>
            <SectionContainer>
              {sections.map((val, idx) => (
                <Section key={idx}>
                  <SectionName>{val}</SectionName>
                  <SectionValue>
                    {val !== 'Abilities'
                      ? pokemon[val.toLowerCase()]
                      : pokemon.abilities.join(', ')}
                  </SectionValue>
                </Section>
              ))}
            </SectionContainer>
            <BottomTitle>Evolutions</BottomTitle>
            <PokemonList pokemons={pokemons} />
          </Container>
        </>
      )}
    </Scaffold>
  );
};

export default PokemonDetail;
