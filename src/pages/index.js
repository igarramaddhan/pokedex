// @flow
import React from 'react';
import PokemonList from '../components/PokemonList';

// dummy
import pokemons from '../pokemons';
import Scaffold from '../components/Scaffold';
import Container from '../components/Container';
import PageTitle from '../components/PageTitle';

const Index = () => (
  <Scaffold>
    <Container>
      <PageTitle>Pokepixl</PageTitle>
      <PokemonList pokemons={pokemons} />
    </Container>
  </Scaffold>
);

export default Index;
