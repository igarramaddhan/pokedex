// @flow
import {type Pokemon} from './types';
const pokemons: Pokemon[] = [
  {
    id: 1,
    height: 4,
    weight: 60,
    caught: false,
    name: 'Pikachu',
    types: ['electric'],
    species: 'pikachu',
    abilities: ['lightning-rod', 'static'],
    image:
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png',
  },
  {
    id: 2,
    height: 10,
    weight: 130,
    caught: false,
    name: 'Ivysaur',
    types: ['grass'],
    species: 'ivysaru',
    abilities: ['chlorophyll', 'overgrown'],
    image:
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/2.png',
  },
  {
    id: 3,
    height: 6,
    weight: 85,
    caught: false,
    name: 'Charmander',
    types: ['fire'],
    species: 'charmander',
    abilities: ['solar-power', 'blaze'],
    image:
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/4.png',
  },
  {
    id: 4,
    height: 4,
    weight: 60,
    caught: false,
    name: 'Pikachu',
    types: ['electric'],
    species: 'pikachu',
    abilities: ['lightning-rod', 'static'],
    image:
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png',
  },
  {
    id: 5,
    height: 7,
    weight: 69,
    caught: true,
    name: 'Bulbasaur',
    types: ['poison', 'grass'],
    species: 'bulbasaur',
    abilities: ['chlorophyll', 'overgrown'],
    image:
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png',
  },
  {
    id: 6,
    height: 20,
    weight: 1000,
    caught: true,
    name: 'Venusaur',
    types: ['poison', 'grass'],
    species: 'venusaur',
    abilities: ['chlorophyll', 'overgrown'],
    image:
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/3.png',
  },
];

export default pokemons;
