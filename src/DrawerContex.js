// @flow
import React from 'react';

type DrawerContextType = {
  active: boolean,
  toggleDrawer: () => void,
};

const DrawerContext = React.createContext<DrawerContextType>({
  active: false,
  toggleDrawer: () => {},
});

export default DrawerContext;
