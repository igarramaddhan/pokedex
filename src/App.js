// @flow
import React from 'react';
import {hot} from 'react-hot-loader/root';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

// import pages here
import Index from './pages/index.js';
import PokemonDetail from './pages/PokemonDetail.js';
import DrawerContext from './DrawerContex.js';

function App() {
  const [isDrawerActive, setIsDrawerActive] = React.useState(false);
  const contextValue = {
    active: isDrawerActive,
    toggleDrawer: () => setIsDrawerActive(!isDrawerActive),
  };
  return (
    <DrawerContext.Provider value={{...contextValue}}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" render={props => <Index {...props} />} />
          <Route
            exact
            path="/pokemon/:id"
            render={props => <PokemonDetail {...props} />}
          />
        </Switch>
      </BrowserRouter>
    </DrawerContext.Provider>
  );
}

export default hot(App);
